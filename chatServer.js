let net = require('net');

// 접속하는 클라이언트들을 담을 배열선언
let clients = [];
// 서버를 만들어 클라이언트로부터 클라이언트 소켓을 받아 서버소켓을 할당
let serverSocket = net.createServer(clientSocket => {
    // 받은 클라이언트 소켓을 배열에 넣는다.
    clients.push(clientSocket);
    console.log('클라이언트가 연결되었습니다.');
    // 클라이언트로 데이터를 받으면
    clientSocket.on('data', buf => {
        // 닉네임을 설정하는 것인지 확인
        if (buf.toString().indexOf('NICKNAME') >= 0) {
            // 닉네임 설정일 경우 클라이언트 닉네임으로 할당
            clientSocket.nickname = buf.toString().split('NICKNAME: ')[1].replace('\r', '').replace('\n', '');
            // 모든 클라이언트에 메시지로 입장한 클라이언트 보내기
            for (let i = 0; i < clients.length; i++) {
                clients[i].write(Buffer.from('MESSAGE: [' + clientSocket.nickname + ']님이 입장하였습니다.\r\n'));
            }
        }
        // 메시지를 받은 것인지 확인
        if (buf.toString().indexOf('MESSAGE') >= 0) {
            // 메시지인 경우 메시지를 표시를 지우고
            let data = buf.toString().replace('MESSAGE: ', '');
            // 보낸 클라이언트의 닉네임과 메시지를 구성하여 할당
            data = '['+ clientSocket.nickname +'] '+ data;
            for (let i = 0; i < clients.length; i++) {
                // 메시지를 보낸 클라이언트을 제외한 클라이언트에 메시지 보내기
                if (clients[i] != clientSocket) {
                    clients[i].write(Buffer.from('MESSAGE: ' + data));
                }
            }
        }
    });
    // 클라이언트과 연결이 종료되면 클라이언트 목록에서 제거한다.
    clientSocket.on('error', () => {
        // 클라이언트 배열에 인덱스가 0 이상이면 소켓이 있는 것으로
        if (clients.indexOf(clientSocket) >= 0) {
            // 클라이언트 소켓에 해당하는 인텍스를 찾아 배열에서 제거하고 그 소켓을 할당
            let removedSocket = clients.splice(clients.indexOf(clientSocket), 1);
            console.log('클라이언트와 연결이 종료되었습니다.');
            for (let i = 0; i < clients.length; i++) {
                // 나머지 클라이언트에 퇴장 메시지 보내기
                clients[i].write(Buffer.from('MESSAGE: [' + removedSocket[0].nickname + ']님이 퇴장하였습니다.\r\n'));
            }
        }
    });
});
// 소버소켓이 자신이 사용할 포트번호를 선택하고 포트를 개방한다.
serverSocket.listen(3000, () => {
    console.log('채팅서버의 3000번 포트가 준비되었습니다.');
});