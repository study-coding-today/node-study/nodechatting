let net = require('net');

// 닉네임 및 서버소켓 변수선언 및 할당
let nickname = null, serverSocket = null;
process.stdout.write(Buffer.from('사용할 닉네임을 입력하세요: '));
// 닉네임을 버퍼로 입력받으면
process.stdin.on('data', buf => {
    // 입력받은 버퍼내용을 가공하여 할당
    let inputData = buf.toString().replace('\r','').replace('\n', '');
    // 입장된 클라이언트인지 닉네임 유무로 확인하여 있으면 메시지로 보내기
    if (nickname) {
        // 버퍼로 입력받은 메시지를 소켓에 싫어 보내기
        serverSocket.write(Buffer.from('MESSAGE: ' + buf.toString()));
    } else {
        // 입장하는 클라이언트로 닉네임을 입력하지 않은 경우
        if (inputData == '') {
            process.stdout.write(Buffer.from('사용할 닉네임을 입력하세요: '));
            return;
        } else if (inputData.indexOf(' ') >= 0) {
            // 닉네임은 입력하였지만 띄어쓰기를 한경우
            console.log('닉네임은 띄어쓰기를 할 수 없습니다.');
            process.stdout.write(Buffer.from('사용할 닉네임을 입력하세요: '));
            return;
        } else {
            // 닉네임을 제대로 입력한 경우
            nickname = inputData;
            // 서버와 연결을 요청하여 서버소켓을 받아 할당
            serverSocket = net.createConnection(3000, '211.197.209.161', () => {
                console.log('채팅서버와 연결되었습니다.');
                // 서버소켓에 닉네임을 실어 보내기
                serverSocket.write(Buffer.from('NICKNAME: ' + nickname + '\r\n'));
                // 서버로부터 데이터를 받은경우
                serverSocket.on('data', buf => {
                    // 버퍼객체를 자바스크립트의 문자열형식으로 변환하여 할당
                    let data = buf.toString();
                    // 메시지를 내화면에 출력하기
                    process.stdout.write(data.replace('MESSAGE: ', ''));
                });
            });
        }
    }
});

